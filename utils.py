import pandas as pd
import numpy as np
import geopandas as gp
from scipy.spatial import cKDTree

# Modelo
from sklearn.pipeline import Pipeline
import joblib


def get_centroid_coords(gdf):
    """
    Función para obtener las coordenadas de un conjunto de polígonos
    """
    coords = np.array(list(gdf.geometry.centroid.apply(lambda x: (x.x, x.y))))
    return coords

def get_neighbors(grids, soportes, k=10):
    """
    Función para buscar los vecinos cercanos mediante kd-tree.
    """
    kd_tree = cKDTree(grids)
    dist, idx = kd_tree.query(soportes, k=k)
    return dist, idx

def get_area_intersection(idx_list, idx_list_grid, grids, soportes, grid_areas=None):
    """
    Función que cálcula el ponderador de cada grid
    """
    if grid_areas is None:
        grid_areas = {id_p: 0 for id_p in grids.id_p}
        
    data = []
    for soporte_id, grid_indices in enumerate(idx_list):
        tmp_soporte = soportes.iloc[[soporte_id]]
        tmp_grids = grids.loc[grid_indices].copy()
        grid_area = tmp_grids.area.mean()    
        
        overlay = gp.overlay(tmp_grids, tmp_soporte, how='intersection')
        overlay['area'] = overlay.geometry.area
        overlay['weight'] = overlay['area'] / grid_area
        
        overlay = overlay[['id_p', 'weight']]
        df = tmp_grids.merge(overlay, on='id_p', how='outer')[['id_p', 'weight']]
        df = df.fillna(0)
        data.append(df)
    
    return data

def suma_ponderada(xdict, wdict):
    s = 0.0
    for key in xdict:
        s += wdict[key]*xdict[key]
    return round(s)
