import pandas as pd
import numpy as np
import geopandas as gp
import dask.dataframe as dd
from scipy.spatial import cKDTree
import argparse
import time
import os

import data_insert_utils
from utils import *

# Modelo
from sklearn.pipeline import Pipeline
import joblib


def predict_rbuffer(soportes):
    """
    Estima el rbuffer para una colección de soportes

    """
    best = ['ALTURA (MTS)', 'BASE (MTS)', 'ESTRUCTURA', 'ILUMINACIÓN', 'POSTE']
    X = soportes[best]  
    modelo = joblib.load('src/modelo_visibilidad.joblib') 
    return modelo.predict(X)

def find_weights(soportes):
    """
    Encuentra los 10 grids más cercanos y asigna a cada uno su ponderador (weight)
    """
    # DataFrame a GeoDataFrame en coordenadas métricas
    soportes= gp.GeoDataFrame(soportes, geometry=\
                    gp.points_from_xy(soportes.LONGITUD, soportes.LATITUD, crs=4326))
    soportes = soportes.to_crs(epsg=6372)
    soportes['rbuffer'] = soportes.buffer(soportes['rbuffer'])
    soportes = soportes.set_geometry('rbuffer')

    # Cargar grids
    #TODO: habria que reemplazarlo por la query a postgre con punto radio
    grids = gp.read_file('data/grids.geojson', crs=4326).to_crs(epsg=6372)
    print('--- GRIDS ---')
    print(grids.head(3))
    # Hallar centroides para grids y soportes
    grids_centroids = get_centroid_coords(grids)
    soportes_centroids = get_centroid_coords(soportes)
    # TODO: quitar esto, pues ya tenemos los 10 grids mas cercanos
    # Hallar 10 grids más cercanos y retícula de 30 grids cercanos
    _, idx = get_neighbors(grids_centroids, soportes_centroids)
    print('--- idx ---')
    print(idx)
    _, grid_id = get_neighbors(soportes_centroids, grids_centroids, k=30)
    # TODO: pasarle los datos de los 10 grids mas cercanos
    # Cálculo de ponderadores
    grid_areas = get_area_intersection(idx, grid_id, grids, soportes)

    # Generar dataframe con grids y ponderadores
    df_weights = []
    for idx_soporte, df in enumerate(grid_areas):
        id_unico = soportes['ID ÚNICO'].iloc[idx_soporte]
        id_unico=[id_unico]*10
        row1, row2 = df.T.values
        df_ = pd.DataFrame()
        df_['ID_UNICO']=id_unico
        df_['id_p']=row1
        df_['id_p']=df_['id_p'].astype(int)
        df_['weight']=row2
        df_=df_[df_.weight!=0]
        df_weights.append(df_)

    return pd.concat(df_weights).reset_index()

def retrieve_metrics(soportes):
    """
    Calcula los hits y users para cada soporte. Supone que el target es el mismo 
    para todos los soportes. Target es un diccionario. 
    """
    # Calcular rbuffer para los soportes
    soportes['rbuffer'] = predict_rbuffer(soportes)

    # Calcular ponderadores para los soportes
    weights = find_weights(soportes)

    return weights
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Modelo de visibilidad')
    parser.add_argument('--filename', default='input.csv',
                        help='Nombre del csv con los soportes; incluye filepath')

    parser.add_argument('--save', default=True,
                        type=str, 
                        help='Guardar en output.csv')

    args = parser.parse_args()
    filename = args.filename
    save = args.save

    soportes = pd.read_csv(filename)

    tic = time.perf_counter()

    soportes = retrieve_metrics(soportes=soportes)

    print('Time:', round((time.perf_counter()-tic)/60, 2), 'min')
    
    if save:
        soportes['auf']=1
        data_insert_utils.insert_table(soportes[['ID_UNICO', 'auf','id_p', 'weight']])
    else:
        soportes
