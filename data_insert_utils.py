import urllib

import pandas as pd
import pymysql

def insert_table(df):
    connection = pymysql.connect(host='infinia-platform-dev.cvccucruyafd.eu-west-1.rds.amazonaws.com',
                                 user='infinia_pre',
                                 password='YXzZe2kGgJ4W83ZY',
                                 db='infinia_outdoor')

    cursor = connection.cursor()
    query = "INSERT INTO `proximity_locations` (`auf`, `idp`, `weight`, `id`) VALUES (%s, %s, %s, %s)"

    for r in df.index:
        id=df.loc[r].values[0]
        auf=df.loc[r].values[1]
        weight=df.loc[r].values[2]
        idp=df.loc[r].values[3]
        cursor.execute(query, (id,auf,weight,idp))

    connection.commit()